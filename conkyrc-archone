# Conky, a system monitor, based on torsmo
#
# Any original torsmo code is licensed under the BSD license
#
# All code written since the fork of torsmo is licensed under the GPL
#
# Please see COPYING for details
#
# Copyright (c) 2004, Hannu Saransaari and Lauri Hakkarainen
# Copyright (c) 2005-2009 Brenden Matthews, Philip Kovacs, et. al. (see AUTHORS)
# All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

alignment br
background no
border_width 1
cpu_avg_samples 2
default_color white
color0 grey
color1 lightgrey
color2 ff3333
color3 3399ff
color4 33ff33
default_graph_size 169 28
default_outline_color white
default_shade_color white
double_buffer yes
draw_borders no
draw_graph_borders no
draw_outline no
draw_shades no
use_xft yes
xftfont inconsolata:size=9
gap_x 5
gap_y 32
if_up_strictness address
maximum_width 362
minimum_size 362 5
net_avg_samples 2
no_buffers yes
out_to_console no
out_to_stderr no
own_window yes
own_window_class Conky
own_window_transparent yes
own_window_type desktop_mem
stippled_borders 0
update_interval 1.0
uppercase no
use_spacer none
show_graph_scale no
show_graph_range no


TEXT
${goto 62} $alignr${scroll 51 $nodename - $sysname $kernel}
${goto 51} ${color0}$hr
${goto 42} ${color0}Uptime: $color$uptime$alignr${color0}Time: $color${time %Z %T}
${goto 33} ${color0}CPU Frequency (in GHz): $color$freq_g$alignr${color0}Date: $color${time %a %d/%m/%y}
${goto 27} ${color0}CPU Usage: $color$cpu% ${cpubar 4}
${goto 20} ${color0}RAM Usage: $color$mem/$memmax - $memperc% ${membar 4}
${goto 15} ${color0}Swap Usage: $color$swap/$swapmax - $swapperc% ${swapbar 4}
${goto 11} ${color0}Processes: $color$processes  ${color0}Running: $color$running_processes
${goto 7} ${color0}Battery: $color${battery_time BAT1} (${battery_percent BAT1}%) ${battery_bar BAT1}
${goto 5} ${color0}Volume: $color${mixer}%$alignr${color0}GMail:${color4} ${execpi 300 python ~/Software/Scripts/python/gmail.py}
${goto 3} ${color0}$hr
${goto 1} ${color0}File systems (Used/Total/Free):
  ${color}/ ${color0}U:$color${fs_used /}/${color0}T:$color${fs_size /}/${color0}F:$color${fs_free /} ${fs_bar 6 /}
  ${color}/home/ ${color0}U:$color${fs_used /home}/${color0}T:$color${fs_size /home}/${color0}F:$color${fs_free /home} ${fs_bar 6 /home}
${if_up wlan0}${goto 1} ${color0}Wi-Fi (${wireless_essid wlan0}): $color${wireless_link_qual wlan0}/${wireless_link_qual_max wlan0} (${wireless_link_qual_perc wlan0}%)$alignr${addr wlan0}
${goto 2}   ${color1}Up: $color${totalup wlan0} @ ${upspeedf wlan0}KiB/s${goto 198}${color1}Down: $color${totaldown wlan0} @ ${downspeedf wlan0}KiB/s
${goto 3}   ${color0}${upspeedgraph wlan0 28,169 ff3333 3399ff -l} $alignr${color0}${downspeedgraph wlan0 28,169 3399ff ff3333 -l}
${goto 9} ${color0}$hr
${goto 20} ${color0}Name ${goto 234}PID   CPU%   MEM%${color}
${goto 20}  ${top_mem name 1}${goto 222}${top_mem pid 1} ${top_mem cpu 1} ${top_mem mem 1}
${goto 29}  ${top_mem name 2}${goto 222}${top_mem pid 2} ${top_mem cpu 2} ${top_mem mem 2}
${goto 38}  ${top_mem name 3}${goto 222}${top_mem pid 3} ${top_mem cpu 3} ${top_mem mem 3}
${goto 47}  ${top_mem name 4}${goto 222}${top_mem pid 4} ${top_mem cpu 4} ${top_mem mem 4}
${goto 58}  ${top_mem name 5}${goto 222}${top_mem pid 5} ${top_mem cpu 5} ${top_mem mem 5}
${goto 71}  ${top_mem name 6}${goto 222}${top_mem pid 6} ${top_mem cpu 6} ${top_mem mem 6}
${goto 87}  ${top_mem name 7}${goto 222}${top_mem pid 7} ${top_mem cpu 7} ${top_mem mem 7}
${else} ${if_up eth0}${goto 1} ${color0}Ethernet: $color${addr eth0}
${goto 2}   ${color1}Up: $color${totalup eth0} @ ${upspeedf eth0}KiB/s${goto 198}${color1}Down: $color${totaldown eth0} @ ${downspeedf eth0}KiB/s
${goto 3}   ${color0}${upspeedgraph eth0 28,169 ff3333 3399ff -l} $alignr${color0}${downspeedgraph eth0 28,169 3399ff ff3333 -l}${else}

${alignc}${color2}No internet connection

${endif}
${goto 9} ${color0}$hr
${goto 20} ${color0}Name ${goto 234}PID   CPU%   MEM%${color}
${goto 20}  ${top_mem name 1}${goto 222}${top_mem pid 1} ${top_mem cpu 1} ${top_mem mem 1}
${goto 29}  ${top_mem name 2}${goto 222}${top_mem pid 2} ${top_mem cpu 2} ${top_mem mem 2}
${goto 38}  ${top_mem name 3}${goto 222}${top_mem pid 3} ${top_mem cpu 3} ${top_mem mem 3}
${goto 47}  ${top_mem name 4}${goto 222}${top_mem pid 4} ${top_mem cpu 4} ${top_mem mem 4}
${goto 58}  ${top_mem name 5}${goto 222}${top_mem pid 5} ${top_mem cpu 5} ${top_mem mem 5}
${goto 71}  ${top_mem name 6}${goto 222}${top_mem pid 6} ${top_mem cpu 6} ${top_mem mem 6}
${goto 87}  ${top_mem name 7}${goto 222}${top_mem pid 7} ${top_mem cpu 7} ${top_mem mem 7}
