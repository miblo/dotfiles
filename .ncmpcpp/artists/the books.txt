The story of The Books began in 2000, when Nick Zammuto and Paul de Jong met through a friend in New York City. Sharing similar interests but different backgrounds in acoustic music and found sound, Zammuto and de Jong took their sonic experiments to the studio. Eventually, with some urging by Tom Steinle of Tomlab Records, they created what would become their debut record, Thought for Food, in 2002. Within a year, the Books relocated to Hot Springs, NC, and recorded and released The Lemon of Pink. With a lot of favorable word of mouth and critical buzz from the first two records, the Books relocated again in winter of 2004 and recorded in an old Victorian home in North Adams, MA. With the release of Lost and Safe in April of 2005, the Books prepared to tour with their unique blend of samples and acoustic music. All three Books albums were released on Tomlab Records. 

For more information, music, etc. on The Books go to:
http://www.myspace.com/thebooksmusicpage

Similar artists:

 * Caribou (http://www.last.fm/music/Caribou)
 * Black Moth Super Rainbow (http://www.last.fm/music/Black+Moth+Super+Rainbow)
 * The Octopus Project (http://www.last.fm/music/The+Octopus+Project)
 * Animal Collective (http://www.last.fm/music/Animal+Collective)
 * Dan Deacon (http://www.last.fm/music/Dan+Deacon)
 * experimental (http://www.last.fm/tag/experimental)
 * indie (http://www.last.fm/tag/indie)
 * electronic (http://www.last.fm/tag/electronic)
 * electronica (http://www.last.fm/tag/electronica)
 * indietronica (http://www.last.fm/tag/indietronica)

http://www.last.fm/music/The+Books