The Urinals were a three-piece art punk band formed in 1978 in Los Angeles. They had a severely pared-down, frenetic sound, sometimes opting for no more than two chords in a song (for example, "Hologram" and "Ack Ack Ack Ack"). They played shows with proto-hardcore luminaries such as Black Flag and Middle Class, and inspired the very influential Minutemen, who covered "Ack Ack Ack" on their live EP Tour-Spiel and their final studio album 3-Way Tie (For Last). (Urinals bassist John Talley-Jones directed a Three Stooges-influenced video for the 3-Way Tie... version of "Ack Ack Ack Ack".)

They released three 7-inch records on their own Happy Squid label over the next two years, which remained relatively rarely heard until the re-release of this material on 1996's Negative Capability CD retrospective on Happy Squid/Amphetamine Reptile. It is currently in print on Warning Label/Happy Squid. In 1981, they changed their name to 100 Flowers, but continued to play music in the minimalist vein of their early songs.  More recently, the Urinals disbanded with the departure of guitarist Rod Barker.  Rob Roberge then joined with Talley-Jones and drummer Kevin Barrett to form The Chairs of Perception.

In addition to their influence on early punk, the Urinals/100 Flowers' songs were also covered by later indie bands like No Age, Halo Of Flies, and Yo La Tengo.

Similar artists:

 * 100 Flowers (http://www.last.fm/music/100+Flowers)
 * Desperate Bicycles (http://www.last.fm/music/Desperate+Bicycles)
 * Electric Eels (http://www.last.fm/music/Electric+Eels)
 * Black Randy & The Metrosquad (http://www.last.fm/music/Black%2BRandy%2B%2526%2BThe%2BMetrosquad)
 * Saccharine Trust (http://www.last.fm/music/Saccharine+Trust)
 * punk (http://www.last.fm/tag/punk)
 * post-punk (http://www.last.fm/tag/post-punk)
 * punk rock (http://www.last.fm/tag/punk%20rock)
 * 70s (http://www.last.fm/tag/70s)
 * lo-fi (http://www.last.fm/tag/lo-fi)

http://www.last.fm/music/Urinals