Until recently there were multiple bands using the name Pivot. 

1) Pivot (United States) progressive rock band from Raleigh, North Carolina featuring Brian Kelly on vocals, guitar, and keys, Eric Hambright on guitar, Mike Hambright on bass, and Phil Cicco on drums. Discography includes: Self Titled EP (1999), Simple Machines (2003), The Dream (2006), 5 Days (2009).

2) Pivot (Australia) - see PVT for artist page, are an experimental post-rock band formed in Sydney in 1999. In May 2010 they changed their name because they received a cease and desist letter from the US Pivot informing them that they were infringing upon a registered US trademark. The Australian band decided not to challenge this legally.



    
User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.

Similar artists:

 * PVT (http://www.last.fm/music/PVT)
 * Seekae (http://www.last.fm/music/Seekae)
 * Triosk (http://www.last.fm/music/Triosk)
 * Nice Nice (http://www.last.fm/music/Nice+Nice)
 * Decoder Ring (http://www.last.fm/music/Decoder+Ring)
 * post-rock (http://www.last.fm/tag/post-rock)
 * experimental (http://www.last.fm/tag/experimental)
 * instrumental (http://www.last.fm/tag/instrumental)
 * australian (http://www.last.fm/tag/australian)
 * electronic (http://www.last.fm/tag/electronic)

http://www.last.fm/music/Pivot