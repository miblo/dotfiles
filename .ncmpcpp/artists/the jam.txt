The Jam was a mod revival group formed in Woking, England in 1972 consisting of Paul Weller (vocals, guitar), Bruce Foxton (bass, vocals) and Rick Buckler (drums). The band split in 1982 and had four UK #1 singles (Going Underground, Start!, Town Called Malice and Beat Surrender).

They were one of the most popular groups of the day, achieving eighteen straight Top 40 singles in the UK from their chart debut in 1977 to their swan song in 1982, including four #1 hits. Incredibly, two of these eighteen singles were available on an import-only basis; they remain the best-selling import singles of all time in the UK. They are almost singularly responsible for the early 1980s mod revival movement, particularly in the U.S. where their popularity among the mod subculture was and remains unrivaled. Frontman Paul Weller has been affectionately known as the modfather and spiritual leader of all mods since the early 1980s.

They also released six albums in their day, the last of which (The Gift) hit #1 on the UK album charts. Massively popular in Britain, as well as much of the rest of Europe and beyond, they never gained much commercial success in North America, but they did retain a considerable cult following. They drew upon a variety of stylistic influences over the course of their career, including punk rock, british invasion, American soul, mod music, and even British psychedelia. Even into the 1990s their music proved to be highly influential on many successful British guitar pop bands from The Smiths in the ’80s to britpop groups Blur and Oasis more recently. Despite the group's limited fame in the United States, they remain a major influence on popular American groups such as Green Day as well. To this day, they rank as one of the most highly successful British groups of all time.

Launching the career of singer, guitarist, and songwriter Paul Weller, who remains a successful solo artist to this day, the trio was known for its high-energy, hard-hitting pop songs, distinctly British flavour, and sharp mod image. All three members were skilled musicians and made critical contributions to the group’s sound. Weller wrote and sang virtually all of the group’s original compositions, and drove them with the chiming tones of a Rickenbacker guitar. Bassist Bruce Foxton made up for the group’s lack of a second guitarist by carrying much of the melody in his distinctive playing, a confluence of the power of John Entwistle with the speed and melodicism of Paul McCartney. His instantly memorable basslines were the foundation of many of the group’s songs, including the hits Down In The Tube Station At Midnight, The Eton Rifles and A Town Called Malice. Foxton's sweet harmonies also gave the group another memorable cachet. Drummer Rick Buckler played with the full-on energy of Keith Moon, but dispensed with Moon’s flashiness in favour of steady but intricate beats and subtle, deft touches. Despite having only three members, The Jam are still regarded as one of the most powerful and fiery groups in British rock.

The JAM are also a EuroDance project.
Their hits are: What's The Way To Your Heart & Thinking About You.
Last.fm has tagged this artist with the same tag words as The Jam.

There's also an Estonian eurodance / pop artist called Jam, but it appears in Last.fm as The Jam. Jam's biggest hits are: See 5 & Olen elult kõik saanud.

And not to forget: the Belgian eurodance artist called Jam, with Zippora de Brouwer on vocals, produced by Basto... With hits like Just Be Yourself and Keep On Movin'
    
User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.

Similar artists:

 * Paul Weller (http://www.last.fm/music/Paul+Weller)
 * The Style Council (http://www.last.fm/music/The+Style+Council)
 * Small Faces (http://www.last.fm/music/Small+Faces)
 * Secret Affair (http://www.last.fm/music/Secret+Affair)
 * The Specials (http://www.last.fm/music/The+Specials)
 * punk (http://www.last.fm/tag/punk)
 * mod (http://www.last.fm/tag/mod)
 * new wave (http://www.last.fm/tag/new%20wave)
 * british (http://www.last.fm/tag/british)
 * rock (http://www.last.fm/tag/rock)

http://www.last.fm/music/The+Jam