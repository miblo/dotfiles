Spring Heel Jack is an English electronic music group consisting of John Coxon and Ashley Wales.

Wales was formerly a composer of "serious" music, an influence that carried over to much of the duo's work. Their first three drum and bass albums released during the 1990s earned positive reviews for their innovate take on the genre. 
More recently they have moved away from drum and bass to collaborate with free improvisers and free jazz players from the United States and Europe, producing three albums for the Thirsty Ear label's Blue Series of adventurous cross-genre efforts. Collaborators have included saxophonists Evan Parker and Tim Berne, bassist William Parker, trumpeters Roy Campbell,Kenny Wheeler and Wadada Leo Smith, trombonist Paul Rutherford, drummer Han Bennink, pianist Matthew Shipp and guitarist Jason Pierce.

In 2006 Coxon and Wales began to operate a record label, Treader, releasing CDs by themselves and other associated artists, including Mark Sanders,John Tchicai,Eddie Prevost and Alex Ward.

Similar artists:

 * William Parker (http://www.last.fm/music/William+Parker)
 * Matthew Shipp (http://www.last.fm/music/Matthew+Shipp)
 * Supersilent (http://www.last.fm/music/Supersilent)
 * The Vandermark 5 (http://www.last.fm/music/The+Vandermark+5)
 * Anthony Braxton (http://www.last.fm/music/Anthony+Braxton)

http://www.last.fm/music/Spring+Heel+Jack