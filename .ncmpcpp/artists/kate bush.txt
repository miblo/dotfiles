Wuthering Heights", which was number one in the British music charts for four weeks.  

        Read more about Kate Bush on Last.fm.
    
    
User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.]]>

Similar artists:

 * Tori Amos (http://www.last.fm/music/Tori+Amos)
 * Joni Mitchell (http://www.last.fm/music/Joni+Mitchell)
 * Björk (http://www.last.fm/music/Bj%C3%B6rk)
 * PJ Harvey (http://www.last.fm/music/PJ+Harvey)
 * Goldfrapp (http://www.last.fm/music/Goldfrapp)
 * female vocalists (http://www.last.fm/tag/female%20vocalists)
 * singer-songwriter (http://www.last.fm/tag/singer-songwriter)
 * pop (http://www.last.fm/tag/pop)
 * alternative (http://www.last.fm/tag/alternative)
 * 80s (http://www.last.fm/tag/80s)

http://www.last.fm/music/Kate+Bush