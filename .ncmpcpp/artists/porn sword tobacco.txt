Porn Sword Tobacco is Henrik Jonsson from Gothenburg.  He has recorded under a few different names, but it was not until summer 2004 that this particular project came together. 

Experimental, ambient, electronic, modern classical and leftfield producer from Sweden, his artist name is apparently the name of a shop at the edge of a forest in his home country, which sells exactly those three products.

Similar artists:

 * Marsen Jules (http://www.last.fm/music/Marsen+Jules)
 * Julien Neto (http://www.last.fm/music/Julien+Neto)
 * Swod (http://www.last.fm/music/Swod)
 * The Boats (http://www.last.fm/music/The+Boats)
 * Dictaphone (http://www.last.fm/music/Dictaphone)
 * ambient (http://www.last.fm/tag/ambient)
 * electronic (http://www.last.fm/tag/electronic)
 * minimal (http://www.last.fm/tag/minimal)
 * experimental (http://www.last.fm/tag/experimental)
 * electronica (http://www.last.fm/tag/electronica)

http://www.last.fm/music/Porn+Sword+Tobacco