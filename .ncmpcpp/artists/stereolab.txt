Stereolab are a UK-based band whose style, mixing 1950s-1960s pop and lounge music with the "motorik" beat of krautrock, was one of the first to which the term "post-rock" was applied. They are noted for the use of vintage keyboard instruments like Moog synthesizers and Vox and Farfisa organs. Stereolab is also notable for founding their own record label, Duophonic Records, with a grant from UK charity The Prince's Trust. The band is often referred to as "The Groop" by their fans (and in the title of their song "The Groop Play Chord X" on the album Space Age Batchelor Pad Music).

They were founded in 1990 by songwriters Tim Gane (guitar, keyboards), formerly of the band McCarthy, and Laetitia Sadier (sometimes credited as Seaya Sadier; vocals, keyboards, trombone, guitar), who is from france and sings in both English and French.

Over the years, Gane and Sadier have enlisted a large number of other musicians to accompany them on stage and on record. The initial line-up featured Martin Kean, formerly of The Chills, on bass, and Joe Dilworth (from their Too Pure label-mates Th' Faith Healers) on drums, with Russell Yates (of Moose) and Mick Conroy (ex-Modern English) also appearing at early live shows. In 1993 they recruited Andy Ramsay (drums), who has remained in the group line-up ever since, and Mary Hansen (vocals, guitar, keyboards, percussion). Hansen's distinctive backing vocals became an important aspect of the Stereolab sound, and she remained a regular feature of the line-up until her death in a cycling accident on December 9, 2002. Multi-instrumentalist Sean O'Hagan of The High Llamas has also been a frequent contributor, particularly with string, brass and keyboard arrangements to the band's studio albums. John McEntire (Tortoise) has also contributed keyboard, electronic effects and studio help over the years. Other members have come and (in some cases) gone over the years, including Duncan Brown, Dave Pajo (from Tortoise), Richard Harrison and Simon Johns (all bass); Gina Morris (vocals); and Katharine Gifford and Morgane Lhote (both on keyboards).

Early Stereolab material displayed a heavy influence of krautrock sounds, particularly Neu! and Faust, characteristically relying on droning, repetitive guitar or keyboard riffs, with or without vocals. Early heavy use of distorted Farfisa combo-organ sounds were also reminiscent of early recordings by The Modern Lovers. As the band developed, they incorporated new instrumentation, and an increasingly complex sense of rhythm and structure, frequently making use of irregular time signatures as well as unorthodox chord progressions and melodic intervals. The band has often made copious use of female backing vocal lines.

Lyrically, Stereolab's music is quirky (song titles evoke memories of 1950s science fiction stories, and are often borrowed directly from old films and records of the period, but have nothing to do with the song's content), but highly politically and philosophically charged, sometimes with a decidedly Surrealist or Situationist bent. (Sadier notes the libertarian Marxist theoretician Cornelius Castoriadis as a particular inspiration.) Sadier's lyrics, in both French and English, often read like highly condensed sociological texts, standing in deliberate and distinct counterpoint to the lush hedonic pop sound of the band. A prime example would be "Ping Pong" from Mars Audiac Quintet, which is an explicit restating of Marxist theory concerning the relationship between economic cycles and war cycles.
Stereolab earned a minor place in the britpop movement, with their sound proving influential to bands like Blur: occasional keyboard-driven b-sides and singer Damon Albarn's love of retro keyboards showed the influence, and in recognition Laetitia Sadier was invited to provide vocals on "To the End" from Parklife.

Despite the band's fan base and critical acclaim, Stereolab has not achieved high levels of financial or popular success. On June 7, 2004, suits at the Warner Music label (to whom the band was signed in the US) announced they were dropping Stereolab in response to the poor sales (40,000 to that date) of Margerine Eclipse. This was part of an ongoing effort by Warner to cut costs; The Breeders and Third Eye Blind were also dropped from the label for this reason. Laetitia Sadier is now also a member of Monade, which is essentially expressive of her own singular musical goals.
    
User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.

Similar artists:

 * Tim Gane (http://www.last.fm/music/Monade)
 * Laetitia Sadier (http://www.last.fm/music/Broadcast)
 * Martin Kean (http://www.last.fm/music/Pram)
 * Joe Dilworth (http://www.last.fm/music/L%C3%A6titia+Sadier)
 * Andy Ramsay (http://www.last.fm/music/The+High+Llamas)
 * Mary Hansen (http://www.last.fm/tag/electronic)
 * Monade (http://www.last.fm/tag/indie)
 * Broadcast (http://www.last.fm/tag/post-rock)
 * Pram (http://www.last.fm/tag/alternative)
 * Lætitia Sadier (http://www.last.fm/tag/electronica)
 * The High Llamas (l version="1.0" encoding="utf-8"?>
<lfm status="ok">
<artist>
  <name>Stereolab</name>
          <mbid>f1df0431-9250-4ec8-95d8-f19ce7cf7fb6</mbid>
            <bandmembers>
                    <member>
                <name>Tim Gane</name>
                                    <yearfrom>1990</yearfrom>
                                            </member>
                    <member>
                <name>Laetitia Sadier</name>
                                    <yearfrom>1990</yearfrom>
                                            </member>
                    <member>
                <name>Martin Kean</name>
                                    <yearfrom>1990</yearfrom>
                                            </member>
                    <member>
                <name>Joe Dilworth</name>
                                    <yearfrom>1990</yearfrom>
                                            </member>
                    <member>
                <name>Andy Ramsay</name>
                                    <yearfrom>1993</yearfrom>
                                            </member>
                    <member>
                <name>Mary Hansen</name>
                                    <yearfrom>1993</yearfrom>
                                                    <yearto>2002</yearto>
                            </member>
            </bandmembers>
        <url>http://www.last.fm/music/Stereolab</url>
  <image size="small">http://userserve-ak.last.fm/serve/34/81318425.png</image>
  <image size="medium">http://userserve-ak.last.fm/serve/64/81318425.png</image>
  <image size="large">http://userserve-ak.last.fm/serve/126/81318425.png</image>
  <image size="extralarge">http://userserve-ak.last.fm/serve/252/81318425.png</image>
  <image size="mega">http://userserve-ak.last.fm/serve/500/81318425/Stereolab.png</image>
  <streamable>1</streamable>
  <ontour>0</ontour>
  <stats>
    <listeners>453966</listeners>
    <playcount>11356001</playcount>
      </stats>

  <similar>
	    <artist>
	  <name>Monade</name>
	  <url>http://www.last.fm/music/Monade</url>
	  <image size="small">http://userserve-ak.last.fm/serve/34/249.jpg</image>
	  <image size="medium">http://userserve-ak.last.fm/serve/64/249.jpg</image>
	  <image size="large">http://userserve-ak.last.fm/serve/126/249.jpg</image>
	  <image size="extralarge">http://userserve-ak.last.fm/serve/252/249.jpg</image>
	  <image size="mega">http://userserve-ak.last.fm/serve/_/249/Monade.jpg</image>
	  
	        	</artist>
    <artist>
	  <name>Broadcast</name>
	  <url>http://www.last.fm/music/Broadcast</url>
	  <image size="small">http://userserve-ak.last.fm/serve/34/58163.jpg</image>
	  <image size="medium">http://userserve-ak.last.fm/serve/64/58163.jpg</image>
	  <image size="large">http://userserve-ak.last.fm/serve/126/58163.jpg</image>
	  <image size="extralarge">http://userserve-ak.last.fm/serve/252/58163.jpg</image>
	  <image size="mega">http://userserve-ak.last.fm/serve/_/58163/Broadcast.jpg</image>
	  
	        	</artist>
    <artist>
	  <name>Pram</name>
	  <url>http://www.last.fm/music/Pram</url>
	  <image size="small">http://userserve-ak.last.fm/serve/34/46157253.png</image>
	  <image size="medium">http://userserve-ak.last.fm/serve/64/46157253.png</image>
	  <image size="large">http://userserve-ak.last.fm/serve/126/46157253.png</image>
	  <image size="extralarge">http://userserve-ak.last.fm/serve/252/46157253.png</image>
	  <image size="mega">http://userserve-ak.last.fm/serve/_/46157253/Pram++MF.png</image>
	  
	        	</artist>
    <artist>
	  <name>Lætitia Sadier</name>
	  <url>http://www.last.fm/music/L%C3%A6titia+Sadier</url>
	  <image size="small">http://userserve-ak.last.fm/serve/34/60113515.png</image>
	  <image size="medium">http://userserve-ak.last.fm/serve/64/60113515.png</image>
	  <image size="large">http://userserve-ak.last.fm/serve/126/60113515.png</image>
	  <image size="extralarge">http://userserve-ak.last.fm/serve/252/60113515.png</image>
	  <image size="mega">http://userserve-ak.last.fm/serve/_/60113515/Ltitia+Sadier+Laetitia+Sadier+02.png</image>
	  
	        	</artist>
    <artist>
	  <name>The High Llamas</name>
	  <url>http://www.last.fm/music/The+High+Llamas</url>
	  <image size="small">http://userserve-ak.last.fm/serve/34/505378.jpg</image>
	  <image size="medium">http://userserve-ak.last.fm/serve/64/505378.jpg</image>
	  <image size="large">http://userserve-ak.last.fm/serve/126/505378.jpg</image>
	  <image size="extralarge">http://userserve-ak.last.fm/serve/252/505378.jpg</image>
	  <image size="mega">http://userserve-ak.last.fm/serve/_/505378/The+High+Llamas.jpg</image>
	  
	        	</artist>
  </similar>
    <tags>
        <tag>
	  <name>electronic</name>
	  <url>http://www.last.fm/tag/electronic</url>
	</tag>
        <tag>
	  <name>indie</name>
	  <url>http://www.last.fm/tag/indie</url>
	</tag>
        <tag>
	  <name>post-rock</name>
	  <url>http://www.last.fm/tag/post-rock</url>
	</tag>
        <tag>
	  <name>alternative</name>
	  <url>http://www.last.fm/tag/alternative</url>
	</tag>
        <tag>
	  <name>electronica</name>
	  <url>http://www.last.fm/tag/electronica</url>
	</tag>
      </tags>
      <bio>
            <published>Sat, 15 Jan 2011 16:26:02 +0000</published>
    <summary><![CDATA[Stereolab are a UK-based band whose style, mixing 1950s-1960s pop and <a href="http://www.last.fm/tag/lounge" class="bbcode_tag" rel="tag">lounge</a> music with the &quot;motorik&quot; beat of <a href="http://www.last.fm/tag/krautrock" class="bbcode_tag" rel="tag">krautrock</a>, was one of the first to which the term &quot;<a href="http://www.last.fm/tag/post-rock" class="bbcode_tag" rel="tag">post-rock</a>&quot; was applied. They are noted for the use of vintage keyboard instruments like Moog synthesizers and Vox and Farfisa organs. Stereolab is also notable for founding their own record label, <a href="http://www.last.fm/label/Duophonic" class="bbcode_label">Duophonic</a> Records, with a grant from UK charity The Prince's Trust. The band is often referred to as &quot;The Groop&quot; by their fans (and in the title of their song &quot;The Groop Play Chord X&quot; on the album <a title="Stereolab - Space Age Batchelor Pad Music" href="http://www.last.fm/music/Stereolab/Space+Age+Batchelor+Pad+Music" class="bbcode_album">Space Age Batchelor Pad Music</a>). ]]></summary>
    <content><![CDATA[Stereolab are a UK-based band whose style, mixing 1950s-1960s pop and <a href="http://www.last.fm/tag/lounge" class="bbcode_tag" rel="tag">lounge</a> music with the &quot;motorik&quot; beat of <a href="http://www.last.fm/tag/krautrock" class="bbcode_tag" rel="tag">krautrock</a>, was one of the first to which the term &quot;<a href="http://www.last.fm/tag/post-rock" class="bbcode_tag" rel="tag">post-rock</a>&quot; was applied. They are noted for the use of vintage keyboard instruments like Moog synthesizers and Vox and Farfisa organs. Stereolab is also notable for founding their own record label, <a href="http://www.last.fm/label/Duophonic" class="bbcode_label">Duophonic</a> Records, with a grant from UK charity The Prince's Trust. The band is often referred to as &quot;The Groop&quot; by their fans (and in the title of their song &quot;The Groop Play Chord X&quot; on the album <a title="Stereolab - Space Age Batchelor Pad Music" href="http://www.last.fm/music/Stereolab/Space+Age+Batchelor+Pad+Music" class="bbcode_album">Space Age Batchelor Pad Music</a>).  They were founded in 1990 by songwriters <a href="http://www.last.fm/music/Tim+Gane" class="bbcode_artist">Tim Gane</a> (guitar, keyboards), formerly of the band <a href="http://www.last.fm/music/McCarthy" class="bbcode_artist">McCarthy</a>, and <a href="http://www.last.fm/music/+noredirect/Laetitia+Sadier" class="bbcode_artist">Laetitia Sadier</a> (sometimes credited as Seaya Sadier; vocals, keyboards, trombone, guitar), who is from <a href="http://www.last.fm/tag/france" class="bbcode_tag" rel="tag">france</a> and sings in both English and French.  Over the years, Gane and Sadier have enlisted a large number of other musicians to accompany them on stage and on record. The initial line-up featured Martin Kean, formerly of <a href="http://www.last.fm/music/The+Chills" class="bbcode_artist">The Chills</a>, on bass, and Joe Dilworth (from their <a href="http://www.last.fm/label/Too+Pure" class="bbcode_label">Too Pure</a> label-mates <a href="http://www.last.fm/music/Th%27+Faith+Healers" class="bbcode_artist">Th' Faith Healers</a>) on drums, with Russell Yates (of <a href="http://www.last.fm/music/Moose" class="bbcode_artist">Moose</a>) and Mick Conroy (ex-<a href="http://www.last.fm/music/Modern+English" class="bbcode_artist">Modern English</a>) also appearing at early live shows. In 1993 they recruited Andy Ramsay (drums), who has remained in the group line-up ever since, and Mary Hansen (vocals, guitar, keyboards, percussion). Hansen's distinctive backing vocals became an important aspect of the Stereolab sound, and she remained a regular feature of the line-up until her death in a cycling accident on December 9, 2002. Multi-instrumentalist Sean O'Hagan of <a href="http://www.last.fm/music/The+High+Llamas" class="bbcode_artist">The High Llamas</a> has also been a frequent contributor, particularly with string, brass and keyboard arrangements to the band's studio albums. <a href="http://www.last.fm/music/John+McEntire" class="bbcode_artist">John McEntire</a> (<a href="http://www.last.fm/music/Tortoise" class="bbcode_artist">Tortoise</a>) has also contributed keyboard, electronic effects and studio help over the years. Other members have come and (in some cases) gone over the years, including Duncan Brown, Dave Pajo (from Tortoise), Richard Harrison and Simon Johns (all bass); Gina Morris (vocals); and Katharine Gifford and Morgane Lhote (both on keyboards).  Early Stereolab material displayed a heavy influence of krautrock sounds, particularly <a href="http://www.last.fm/music/Neu!" class="bbcode_artist">Neu!</a> and <a href="http://www.last.fm/music/Faust" class="bbcode_artist">Faust</a>, characteristically relying on droning, repetitive guitar or keyboard riffs, with or without vocals. Early heavy use of distorted Farfisa combo-organ sounds were also reminiscent of early recordings by <a href="http://www.last.fm/music/The+Modern+Lovers" class="bbcode_artist">The Modern Lovers</a>. As the band developed, they incorporated new instrumentation, and an increasingly complex sense of rhythm and structure, frequently making use of irregular time signatures as well as unorthodox chord progressions and melodic intervals. The band has often made copious use of female backing vocal lines.  Lyrically, Stereolab's music is quirky (song titles evoke memories of 1950s science fiction stories, and are often borrowed directly from old films and records of the period, but have nothing to do with the song's content), but highly politically and philosophically charged, sometimes with a decidedly Surrealist or Situationist bent. (Sadier notes the libertarian Marxist theoretician Cornelius Castoriadis as a particular inspiration.) Sadier's lyrics, in both French and English, often read like highly condensed sociological texts, standing in deliberate and distinct counterpoint to the lush hedonic pop sound of the band. A prime example would be &quot;<a title="Stereolab &ndash; Ping Pong" href="http://www.last.fm/music/Stereolab/_/Ping+Pong" class="bbcode_track">Ping Pong</a>&quot; from <a title="Stereolab - Mars Audiac Quintet" href="http://www.last.fm/music/Stereolab/Mars+Audiac+Quintet" class="bbcode_album">Mars Audiac Quintet</a>, which is an explicit restating of Marxist theory concerning the relationship between economic cycles and war cycles. Stereolab earned a minor place in the <a href="http://www.last.fm/tag/britpop" class="bbcode_tag" rel="tag">britpop</a> movement, with their sound proving influential to bands like <a href="http://www.last.fm/music/Blur" class="bbcode_artist">Blur</a>: occasional keyboard-driven b-sides and singer <a href="http://www.last.fm/music/Damon+Albarn" class="bbcode_artist">Damon Albarn</a>'s love of retro keyboards showed the influence, and in recognition Laetitia Sadier was invited to provide vocals on &quot;<a title="Blur &ndash; To the End" href="http://www.last.fm/music/Blur/_/To+the+End" class="bbcode_track">To the End</a>&quot; from <a title="Blur - Parklife" href="http://www.last.fm/music/Blur/Parklife" class="bbcode_album">Parklife</a>.  Despite the band's fan base and critical acclaim, Stereolab has not achieved high levels of financial or popular success. On June 7, 2004, suits at the Warner Music label (to whom the band was signed in the US) announced they were dropping Stereolab in response to the poor sales (40,000 to that date) of <a title="Stereolab - Margerine Eclipse" href="http://www.last.fm/music/Stereolab/Margerine+Eclipse" class="bbcode_album">Margerine Eclipse</a>. This was part of an ongoing effort by Warner to cut costs; <a href="http://www.last.fm/music/The+Breeders" class="bbcode_artist">The Breeders</a> and <a href="http://www.last.fm/music/Third+Eye+Blind" class="bbcode_artist">Third Eye Blind</a> were also dropped from the label for this reason. Laetitia Sadier is now also a member of <a href="http://www.last.fm/music/Monade" class="bbcode_artist">Monade</a>, which is essentially expressive of her own singular musical goals.
    
User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.]]></content>
                                <yearformed>1990</yearformed>
            <formationlist>
                    <formation>
                <yearfrom>1990</yearfrom>
                <yearto></yearto>
            </formation>
            </formationlist>
      </bio>
  </artist></lfm>
)
 * electronic (http://www.last.fm/music/Stereolab)
 * indie (http://www.last.fm/music/Monade)
 * post-rock (http://www.last.fm/music/Broadcast)
 * alternative (http://www.last.fm/music/Pram)
 * electronica (http://www.last.fm/music/L%C3%A6titia+Sadier)

http://www.last.fm/music/Stereolab