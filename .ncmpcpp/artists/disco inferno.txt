Disco Inferno was a band formed in Essex, UK in the late 1980s by Ian Crause (guitar & vocals), Paul Wilmott (bass), Rob Whatley (drums) and Daniel Gish (keyboards).

After the departure of Gish (who would later join Bark Psychosis) the three-piece Disco Inferno recorded the single 'Entertainment' with producer Charlie McIntosh. Their first album, Open Doors, Closed Windows, was released in 1991 on Che and received positive reviews, although most mentioned the heavy influence of late 1970s post punk bands, particularly Joy Division and Wire (In fact Disco Inferno's manager, Michael Collins, had previously managed Wire during their 70's incarnation). 'Entertainment', 'Open Doors, Closed Windows' and the 'Science' EP would later be collected on the album In Debt.

In 1992 the band released Summer's Last Sound, widely regarded as the first in a run of classic Disco Inferno EPs which saw the band's increasing use of samples. The band's use of sampling combined with traditional instruments - particularly Wilmott's bass - continued on the EPs a rock to cling to and The Last Dance and reached it's peak on their second album D.I. Go Pop. After the full-on assault of D.I. Go Pop the band opted for restraint on the beautiful Second Language EP which also had a new-found optimism in Crause's lyrics.

Their final single, 'It's A Kid's World', sampled the distinctive drumbeat from Iggy Pop's Lust for Life and added in a series of old children's TV themes to good effect, but despite critical acclaim the band attracted little commercial success and split before their final album Technicolour was released in 1996.


Similar artists:

 * Bark Psychosis (http://www.last.fm/music/Bark+Psychosis)
 * A.R. Kane (http://www.last.fm/music/A.R.+Kane)
 * Long Fin Killie (http://www.last.fm/music/Long+Fin+Killie)
 * Flying Saucer Attack (http://www.last.fm/music/Flying+Saucer+Attack)
 * Hood (http://www.last.fm/music/Hood)
 * post-rock (http://www.last.fm/tag/post-rock)
 * experimental (http://www.last.fm/tag/experimental)
 * shoegaze (http://www.last.fm/tag/shoegaze)
 * post-punk (http://www.last.fm/tag/post-punk)
 * indie (http://www.last.fm/tag/indie)

http://www.last.fm/music/Disco+Inferno